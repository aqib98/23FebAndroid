const express = require('express');
const router = express.Router();
const sqlite3 = require('sqlite3');
const csv =require('csvtojson');
var csvFilePath ='./studentDetails.csv';
var db = new sqlite3.Database('./DBs/TrainingDatabase');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send(db);
});

router.get('/importFromCSV', function(req, res, next){
	var jsonCSV =[];
	csv()
	.fromFile(csvFilePath)
	.on('json',(jsonData)=>{
		jsonCSV.push(jsonData)
	    //
	})
	.on('done',(error)=>{
		create_table(jsonCSV);
	    console.log('end')
	})
	
  	res.json({msg:"respond with a resource"});
});

router.get('/createTable', function(req, res, next) {
	db.run('CREATE table exercise ( StudentID number, Name varchar(10),	Address varchar(20), ExerciseID number,	Description varchar(20), Domain varchar(10));',
	(err, result)=>
	{
		if(err){
			console.log(err);
		} else if(result){
			console.log(result);
			res.json({msg:result});
		}
	})
});

router.get('/insertIntoTable', function(req, res, next) {

	let sql = 'INSERT INTO exercise VALUES (\'Dummy\', \'Dummy\', \'Dummy\', \'Dummy\', \'Dummy\', \'Dummy\')';
	console.log(sql);

	db.run(sql, function(err) {
	  if (err) {
	    return console.error(err.message);
	  }
	  console.log(`${this.changes} Rows inserted `);
	  res.json({msg:`${this.changes} Rows inserted `});

	});
});

router.get('/viewTable', function(req, res, next) {
	db.all('SELECT * from exercise;',
	(err, rows)=>
	{
		if(err){
			console.log(err);
			res.send(err);
		} else if(rows){
			console.log(rows);
			res.send(rows);
		}
	})
});

router.get('/deleteFromTable', function(req, res, next) {
	db.run('DELETE from exercise;', function(err){
		if(err){
			console.log(err);
			res.send(err);
		} else {
			console.log(`${this.changes} Rows inserted `);
			res.json({msg:`${this.changes} Rows deleted `});
		}
	})
});

function create_table(json) {

  // hard coding is cheating, mostly... make it dynamic!

  db.serialize( () => {
    db.run('CREATE table if not exists '
          + 'exercise ('
          + 'StudentID number, '
          + 'Name varchar(10), '
          + 'Address varchar(20), '
          + 'ExerciseID number, '
          + 'Description varchar(20), '
          + 'Domain varchar(10)');

    db.run('delete from exercise'); //or drop the table first..

    var stmt = db.prepare('insert into exercise values (?,?,?,?,?,?)');

    json.forEach( (item) => {
      stmt.run([item.StudentID, item.Name, item.Address, item.ExerciseID, item.Description, item.Domain]);
    });
    console.log(stmt);
    stmt.finalize();

  });

}
process.on('SIGINT', () => {
    db.close();
   
});
module.exports = router;
