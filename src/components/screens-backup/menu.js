import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet, Text, View, TouchableHighlight, Image, TouchableOpacity } from 'react-native';
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import { onSignIn,onSignOut,isSignedIn } from '../../config/auth';
import { getProfileDetails } from '../template/SQLiteOperationsOffline.js';
import { updateProfile } from '../template/api.js';
import { AsyncStorage, BackAndroid } from "react-native";


export default class Menu extends Component {

    constructor(props) {
        super(props);
      this.state={
        profile:[],
        height:null,
        weight:null,
        gender:'Male',
        userid:null,
        genderIndex:null,
        enable:null,
        signedIn: false,
        checkedSignIn: false
      }        
    }

  async componentWillMount(){
    console.log("profile")
    const { checkedSignIn, signedIn } = this.state;
    this.getStudentID();  

  }


     state ={
       data:""
     }

     _moveCourse = async () => {
         const { navigate  } = this.props.navigation;
         navigate("Courses");
     }

     _moveNotification = async () => {
         const { navigate  } = this.props.navigation;
         navigate("Courses");
     }

     _moveProfile = async () => {
         const { navigate  } = this.props.navigation;
         navigate("Profile")
     }

     _moveLogout = async () => {
         onSignOut();
         const { navigate  } = this.props.navigation;
         navigate("SignedOut")
         
     }

  async getStudentID () {
  console.log("ff") 
    let USER_ID = await AsyncStorage.getItem("USER_ID"); 
    this.setState({userid:USER_ID,enable:false},()=>{
      this._getStudentDetails(this.state.userid);
    });
  }
  _getStudentDetails=(id)=>{
    getProfileDetails(id).then(response=>{
      console.log(response)
      if(response.status){
        this.setState({profile:response.data},()=>{
          this.setState({height:this.state.profile.height_in_feet, weight:this.state.profile.current_weight,gender:this.state.profile.gender},()=>{
            if(this.state.gender==="Male"){
              this.setState({genderIndex:0});
            }else if(this.state.gender==="Female"){
              this.setState({genderIndex:1});
            }else{
              this.setState({genderIndex:null});
            }
          });
        });
        console.log(this.state)
      }else{
        ToastAndroid.show(response.show, ToastAndroid.SHORT);
      }
    },err=>{
      console.log(err)
    })
  }


  openMenu = async() => {

    const { navigate  } = this.props.navigation;
    navigate("Courses");

  }


    render() {
        return (
            <View style={styles.container}>

            <View style={styles.header}>

              <View style={styles.menupartleft}>

                  <TouchableOpacity onPress={() => this.openMenu() } >
                    <Text>
                    <Image style={styles.avatar} source={require('./../../Images/1280_back_arrow.png')}/>
                    </Text>
                  </TouchableOpacity>

              </View>
              <View style={styles.menupartright}>

              <Text style={styles.title}>Menu</Text>

              </View>

            </View>

               
                <View>

                <Text onPress={() => this._moveProfile() } style={styles.item}> <Text> Edit Profile  </Text> </Text>

                <Text onPress={() => this._moveNotification() } style={styles.item}> <Text> Courses </Text>  </Text>

                <Text onPress={() => this._moveLogout() } style={styles.item}> <Text> Logout  </Text> </Text>

                </View>

            </View>

        );
    }
}

const styles = StyleSheet.create({
   container: {
   justifyContent: 'center',
  },
  item: {
    fontSize: 22,
    margin: 10,
    color: '#000',
    borderColor: '#000',
    borderBottomWidth: 2,
  },
  status: {
    textAlign:'right',
    fontWeight: 'bold'
  },
  profile: {
    height: 100,
    width: 100,
    marginLeft: 15,
    marginTop:10,
    borderRadius:100,
    borderColor:'#fff',
    borderWidth:2
  },
  menubody: {
    marginTop: 30
  },
  menuheaderright: {
    width: '60%',
    alignSelf: 'flex-end',
    marginTop: -80,
  },
    menuheaderleft: {
    width: '40%',
  },
  text: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#fff'
  },
  textemail: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fff'
  }, 
  header: {
    height: 60,
    backgroundColor: '#262626',
    width: '100%',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOpacity: 0.2 
  },
  progressContainer: {
    alignItems: "flex-end", 
    width: '48%',
    marginTop: -5,
  },  
  avatar: {
    height: 40,
    width: 60,
    marginLeft: 35,
    marginTop: 10
  },
  title: {
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 10,
    marginTop: -10,
  },
  menupartleft: {
    width: '15%',
    marginLeft: 15,
    marginTop: 20
  },
  menupartright: {
    width: '85%',
    marginTop: 24,
  },
  textcenter: {
    justifyContent: 'center',
    textAlign: 'center'
  }
})

module.exports = Menu;
