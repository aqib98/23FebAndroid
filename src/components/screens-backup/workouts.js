import React, { Component } from 'react';
import { AppRegistry, FlatList, StyleSheet, Text, View, TouchableHighlight,  Animated , TouchableOpacity, Image, ScrollView } from 'react-native';
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import { onSignIn } from '../../config/auth';
import { getWorkouts } from '../template/api.js'
import { getStudentDetails } from '../template/SQLiteOperationsOffline.js';

class ProgressBar extends Component {
  
  componentWillMount() {
    this.animation = new Animated.Value(this.props.progress);
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.progress !== this.props.progress) {
      Animated.timing(this.animation, {
        toValue: this.props.progress,
        duration: this.props.duration
      }).start();
    }
  }
  
  
  render() {
    const {
      height,
      borderColor,
      borderWidth,
      borderRadius,
      barColor,
      fillColor,
      row
    } = this.props;

    const widthInterpolated = this.animation.interpolate({
      inputRange: [0, 1],
      outputRange: ["0%", "100%"],
      extrapolate: "clamp"
    })

    return (
      <View style={[{flexDirection: "row", height }, row ? { flex: 1} : undefined ]}>
        <View style={{ flex: 1, borderColor, borderWidth, borderRadius}}>
          <View
            style={[StyleSheet.absoluteFill, { backgroundColor: fillColor }]}
          />
          <Animated.View
            style={{
              position: "absolute",
              left: 0,
              top: 0,
              bottom: 0,
              width: widthInterpolated,
              backgroundColor: barColor
            }}
          />
        </View>
      </View>
    )
  }
}  

ProgressBar.defaultProps = {
  height: 6,
  borderWidth: 0,
  borderRadius: 0,
  barColor: "#3bafda",
  fillColor: "#d8d8d8",
  duration: 70
}

export default class Workouts extends Component {

    constructor(props) {
      super(props);
      this.state={
        workouts:[],
        instructor:{},
        mode:true,
        loader:'0',
        timePassed: false,
        loader:null,
        Dates:[],
        NoOfDays:[],
        selectedCourse:[]
      }
  }
  componentWillMount(){
    var { courseID, user_id, course } = this.props.navigation.state.params;
    this.setState({loader:true, selectedCourse:course})
    this._getAllWorkouts(courseID, user_id);
    console.log(this.props.navigation.state.params)
  }
  componentWillReceiveProps(nextProps){
    //console.log(nextProps)
  }
  componentWillUpdate(nextProps, nextState){
    //console.log(nextProps, nextState)
  }
  _getAllWorkouts=(courseID, user_id)=>{
     this.setState({loader:'0'})
    getWorkouts(courseID, user_id).then(response=>{
      console.log(response);
       this.setState({loader:'1'})
      if(response.mode){
        this.setState({workouts:response.workouts.data,instructor:response.instructor, mode:response.mode},()=>{
          console.log(this.state.workouts, this.state.instructor)
        });
      }else{
        this.setState({workouts:response.data, mode:response.mode},()=>{
          console.log(this.state.workouts, this.state.instructor)
        });
      }

          for(let j=0;j<this.state.workouts.length;j++)
          {

            if(j%2==0)
            {
              this.state.workouts[j].color = '#FFF';
            }
            else
            {
              this.state.workouts[j].color = '#f8f8f8';
            }

          }
          var temp1 = [];
          response.workouts.data.map((workout)=>temp1.push(workout.workout_day))
          const temp2 = temp1.sort(function(a, b) {return a - b});
          var Days = temp2.filter(function(elem, index, self) {
            return index == self.indexOf(elem);
          });
          this.setState({ NoOfDays:Days},()=>{
            let start = new Date(this.state.selectedCourse.start_date);
            let end = new Date(this.state.selectedCourse.end_date);
            var dates = this.getDateArray(new Date(this.state.selectedCourse.start_date.substr(0,10)), new Date(this.state.selectedCourse.end_date.substr(0,10)));
          this.setState(this.state.workouts);
          this.setState({loader:false, Dates:dates})
        })
    },err=>{
      console.log(err)
    })
  }
  getDateArray = (start_date, end_date)=>{
    var arr = new Array();
    var dt = new Date(start_date);
    while(dt <= end_date){
      arr.push(new Date(dt));
      dt.setDate(dt.getDate()+1);
    }
    return arr;
  }
  /*authenticating starts*/
  _viewExercises = async (id, title, day) => {
      const { navigate } = this.props.navigation;
      var { courseID, user_id, courseTitle, course } = this.props.navigation.state.params;
      navigate("Exercises", { workoutID: id, workoutTitle: title, user_id:user_id, courseID: courseID, courseTitle: courseTitle, workout_day:day, course:course });
  }
  /*authenticating starts*/
    _showInstructor=()=>{
      const { navigate } = this.props.navigation;
      var { instructor } = this.state
      console.log(instructor);
      var temp = [];
      temp.push(instructor)
      navigate("Instructer", { instructor: temp, id: instructor.id});
    }


  openMenu = async() => {

    const { navigate  } = this.props.navigation;
    navigate("Courses");

  }
  render() {
    const { NoOfDays, Dates, workouts} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.menupartleft}>
            <TouchableOpacity onPress={() => this.openMenu() } >
              <Text>
                <Image style={styles.avatar} source={require('./../../Images/1280_back_arrow.png')}/>
              </Text>
            </TouchableOpacity>
          </View>
          <View style={styles.menupartright}>
            <Text style={styles.title}>{this.props.navigation.state.params.courseTitle}</Text>
          </View>
        </View>
        <View style={styles.body}>
          <ScrollView style={styles.scroll}>
            <TouchableOpacity  onPress={() => this._showInstructor() }>
              <View style={{margin: 10}}> 
                <Text style={{fontSize: 25, color: '#ef7e2d', textAlign: 'center'}}> 
                  {this.state.instructor.first_name} {this.state.instructor.last_name}
                </Text> 
                <Text style={{textAlign: 'center'}}> Instructor </Text> 
              </View>
            </TouchableOpacity>
            {this.state.loader ? 
            <Image style={{flex:1,alignSelf:'center',width: 100, height: 100,marginTop: 50}} source={require('./../../Images/loading.gif')} />
            :
            this.state.workouts.length?
            NoOfDays.map((day, indexDay)=>
              <View key={indexDay}>
                <Text style={{margin: 10, fontSize: 20, fontWeight: 'bold'}}>Day: {Number(day)+1} Date: {String(Dates[indexDay]).substr(4,11)}</Text> 
                  {workouts.map((workout, indexWorkout)=>
                    {
                      if(day==workout.workout_day){
                      return(
                      <View key={indexWorkout} style={styles.lists} style={{backgroundColor:workout.color,flex: 1,flexDirection: 'row',borderColor: '#f5f4f4',borderBottomWidth:1,borderTopWidth: 1,shadowOffset: { width: 100, height: 100 },shadowColor: '#000',shadowOpacity: 10,shadowRadius: 10}}>
                        <View style={styles.innerlist} style={{margin: 12,marginLeft: 10}}>            
                          <TouchableOpacity  onPress={() => this._viewExercises(workout.id, workout.title, day) }>
                            <Text style={styles.firsttext}> 
                              {workout.title.length<20?workout.title:workout.title.substring(0,20)+'...'}   
                            </Text>
                            <View style={styles.lowertext}>
                              <View style={styles.innertext}> 
                                <Text>
                                  ({workout.exerciseCount}) | { workout.workout_status == '0' ? ' Not Yet Started ' : workout.workout_status == '100' ? ' Completed ' : ' In Progress ' }
                                </Text> 
                              </View>
                              <View style={styles.progressContainer}>
                                <View style={styles.length}>
                                  <ProgressBar row progress={workout.workout_status/100}/>
                                  <Text style={{alignSelf: 'flex-end'}}>{ workout.workout_status == '100'? '100': workout.workout_status}%</Text>  
                                </View>  
                              </View>     
                            </View>   
                          </TouchableOpacity> 
                        </View>
                      </View>)
                      }else return null
                    }
                  )}
              </View>
            )
            :
            <View style={{flex: 1,margin: 10, marginTop: 30}}>
              <Text style={{fontSize: 28, textAlign: 'center'}}>
                No Workouts Under This Course or check your internet connection
              </Text>
            </View>}
          </ScrollView>
        </View>
      </View>  
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
   justifyContent: 'center',
  },
  scroll: {
    flex: 1
  },
  firsttext: {
    fontSize: 20,
    fontWeight:'bold'
  },
  innertext: {
    marginTop: 0,
    width: '50%'
  },
  body: {
    flex : 1,
    justifyContent: 'center'
  },
  list: {
    margin: 1,
    flex: 1,   
  },
  lists: {
    flex: 1, 
    flexDirection: 'row',
    borderColor: 'red',
    borderBottomWidth: 2,
    shadowOffset: { width: 100, height: 100 },
    shadowColor: '#000',
    shadowOpacity: 10,
    shadowRadius: 10
  },
  lowertext: {
    flexDirection: 'row',
    flex: 1,
    width: '100%',
    margin: 4
  },
  length: {
    width: '65%',
    alignItems: 'flex-start',
    
  },
  leftitem: {
    fontSize: 22,
    color: '#000',    
    textAlign: 'left',
    width: '67%'
  },
  rightitem: {
    fontSize: 16,
    paddingVertical: 5,
    color: '#000',
    marginLeft: 10,
    textAlign: 'left',
    width: '34%',
    alignSelf: 'flex-end'
  },   
  status: {
    textAlign:'right',
    fontWeight: 'bold'
  },
  header: {
    height: 60,
    backgroundColor: '#262626',
    width: '100%',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOpacity: 0.2 
  },
  progressContainer: {
    alignItems: "flex-end", 
    width: '48%',
    marginTop: -5,
  },  
  avatar: {
    height: 40,
    width: 60,
    marginLeft: 35,
    marginTop: 10
  },
  title: {
    color: '#fff',
    fontSize: 24,
    fontWeight: 'bold',
    marginLeft: 10,
    marginTop: -10,
    textAlign: 'left'

  },
  menupartleft: {
    width: '13%',
    marginLeft: 15,
    marginTop: 20
  },
  menupartright: {
    width: '87%',
    marginTop: 24,
  },
  textcenter: {
    justifyContent: 'center',
    textAlign: 'center'
  }
})


module.exports = Workouts;
