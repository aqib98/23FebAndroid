import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Switch,
  Image,
  PixelRatio,
  KeyboardAvoidingView,
  ScrollView,
  TouchableHighlight,
  TouchableOpacity,
  TextInput,
  AsyncStorage,
  Dimensions,
  Keyboard,
  ToastAndroid,
  Alert,
  ActivityIndicator
} from 'react-native';
import { NavigationActions } from 'react-navigation';
import { onSignIn,onSignOut, getAllAsyncStroage } from '../../../config/auth';
import { subscribeByStudent } from '../../template/Courses/courses.js'
import axios from 'axios';
import FCM, {FCMEvent, RemoteNotificationResult, WillPresentNotificationResult, NotificationType} from 'react-native-fcm';
import styles from '../Tabs/style.js';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
var { width, height } = Dimensions.get('window');
export default class Login extends Component {

  constructor(props) {
    super(props);
    this.state = {
      course:{},
      loader:false
    };
  }

  componentWillMount(){
    var { course } = this.props.navigation.state.params;
    this.setState({course:course})
  }
  
  _subscribe=async ()=>{
    const { navigate } = this.props.navigation;
    let { course } = this.state
    let USER_ID = await AsyncStorage.getItem("USER_ID");
    console.log('hi');
    var temp=[];
    temp.push({course_id: course.id, 'user_id': USER_ID, data:course.days});
    console.log(temp)
    this.setState({loader:true})
    subscribeByStudent(temp)
    .then(res=>{
      console.log(res);
      this.setState({loader:false})
      ToastAndroid.show(res.show, ToastAndroid.SHORT);
      navigate('UpcomingWorkouts')
    }, err=>{
      this.setState({loader:false})
      console.log(err);
      ToastAndroid.show("Server problem. try again later", ToastAndroid.SHORT);
    })
  }

  render(){
    const { course } = this.props.navigation.state.params;
    const { loader } = this.state;
    return(
      <View style={ styles.mainBody }>

            <View style={styles.listofWrkouts1}>
              <View style={styles.chevron_left_icon}>
                <TouchableOpacity >
                    <FontAwesome name="chevron-left" size={25} color="#FF7E00"   />
                </TouchableOpacity>
              </View>

              <View style={styles.header}>
                    <Text style={styles.topSignupTxt}>
                      SUBSCRIBE
                    </Text>
              </View>
            </View>

            <View style={{marginTop: 100}}>
              <Text style={{fontSize: 30, textAlign: 'center', color: 'white'}}>
                Course : {course.title}
              </Text>
              <Text style={{fontSize: 30, textAlign: 'center', color: 'white'}}>
                Description : {course.description}
              </Text>
            </View>
        <View style={styles.footer}>
          <TouchableOpacity onPress={() => this._subscribe() }>
            <View style={styles.save_view}>
              {loader?
                <ActivityIndicator
                  animating = {loader}
                  color = '#bc2b78'
                  size = "large"
                  style = {styles.activityIndicator}
                />
                :
                <Text style={styles.save_btnTxt}>Subscribe</Text>
              }
            </View>
            <Image style={styles.save_btnImg} source={{ uri: 'buttonimg' }} />
         </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styless = StyleSheet.create({
  courseDetailsContainer:{
    backgroundColor: '#fb620c',
    alignItems: 'center'
  },
  subscribeText: {
    borderWidth: 1,
    padding: 25,
    borderColor: 'black',
    backgroundColor: 'red'
   }
});

