import {
  StyleSheet,
} from 'react-native';
const Dimensions = require('Dimensions');
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

const slideHeight = viewportHeight * 0.4;
const slideWidth = wp(90);
const itemHorizontalMargin = wp(2);
const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;

const styles = StyleSheet.create({

  btm_tabNavigation:{
    position:'absolute',
    bottom:0,
    backgroundColor:'#414249',flex:1,flexDirection:'row',paddingTop:12,paddingBottom:2
  },
  save_btnTxt : {
     fontSize:20,
     paddingTop:20,
     position:'absolute',
     fontWeight:'bold',
     textAlign:'center',
     color:'#fff',
     justifyContent: 'center',
     alignItems: 'center'},
   saveBtn2:{
        marginTop : 90,
        alignItems:'center', position:'absolute'
      },
  upcomingWorkouts_main_body : {
    backgroundColor: '#2d2e37',
    marginTop : 0,
    padding:0,
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width
  },
  upcomingWorkouts_bg_img_view : {
    position: 'absolute',
    top: 0,
    left: 0,
    width: undefined,
    height: undefined,
  },
  upcomingWorkouts_bg_img : {
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  upcomingWorkouts_heading :{
    color:'#ffffff',
    textAlign:'center', marginTop:5,
    fontSize : 20,
    fontWeight:'bold',paddingTop:15
  },
  upcomingWorkouts_scrollView : {
    marginTop : 5,
    marginBottom:170
  },
  upcomingWorkouts_content_view : {
    alignItems:'center'
  },
  upcomingWorkouts_content : {
    width:'75%'
  },
  tuesday_heading:{
    fontSize:12,
    color:'#ffffff',marginTop:0
  },
  whiteline:{
    borderBottomWidth:3,borderBottomColor : '#FFF',marginBottom:7, marginTop:7
  },
  wat_log_track_icons:{
    flex:1,flexDirection:'row',paddingLeft:20,paddingRight:20,paddingTop:20,
    position:'absolute', bottom:100
  },
  cross_icon:{
    position:'absolute',
    bottom:28,
    zIndex:3,
    flex:1,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center',
    width:Dimensions.get('window').width
  },
  trans_bg:{
    flex:1,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center',
    position:'absolute',
    bottom:0,
    height:90,zIndex:2,
    width:Dimensions.get('window').width
  },
  btm_tabNavigation:{
    position:'absolute',
    bottom:0,
    backgroundColor:'#414249',flex:1,flexDirection:'row',paddingTop:10,paddingBottom:4
  },

  //Trackactivity styles
  trackactivity_container :{
    backgroundColor: '#2d2e37',
    marginTop : 0,
    padding:0,
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width
  },
  trackactivity_image_container : {
    position: 'absolute',
     top: 0,
     left: 0,
     width: undefined,
     height: undefined
  },
  trackactivity_bg_image:{
    resizeMode: 'stretch',
    height: Dimensions.get('window').height,
    width: Dimensions.get('window').width,
  },
  trackactivity_heading : {
    color:'#ffffff',
    textAlign:'left', marginTop:5, textAlign:'center', paddingBottom:20,
    fontSize : 20,
    fontWeight:'bold',paddingTop:15
  },
  trackactivity_heartimg_heartrate_viewc : {
    position:'relative'
  },
  trackactivity_heart_img : {
    width:96,
    height:89,
    marginLeft:'6%',
     marginTop:50,
  },
  trackactivity_heartrate : {
    color:'#ffffff',
    textAlign:'left', marginTop:5, textAlign:'center', paddingBottom:20,
    fontSize : 134,
    fontWeight:'bold',paddingTop:15, position:'absolute', top:-6, right:20
  },
  trackactivity_subSection_view1 : {
    marginTop: 30, flexDirection:'row', width:'100%', height:100
  },
  trackactivity_subSection_view2:{
    marginTop:0, flexDirection:'row', width:'100%',
  },
  trackactivity_subSection_view_part1 : {
    alignItems: 'center', width:'48%'
  },
  trackactivity_subSection_view_part1_headingtext:{
    fontWeight:'bold',color:'#fff',fontSize:20,fontFamily:'arial',
    position:'relative', top:-10
  },
  trackactivity_subSection_view_part1_calories:{
    fontWeight:'bold',color:'#fff',fontSize:50,fontFamily:'arial',
  },
  trackactivity_start_stop_btn_view : {
    width:'100%',alignItems: 'center',marginTop: 30,
  },
  trackactivity_start_stop_btn_touchable_opacity : {
    width:'70%', borderColor:'#fff', borderWidth:4,  padding:10,alignItems: 'center'
  },
  trackactivity_start_stop_btn_text : {
    color:'#fff',fontSize:30,fontWeight:'bold'
  },
  trackactivity_footer_img_view : {
    position:'absolute',
    bottom:28,
    zIndex:3,
    flex:1,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center',
    width:Dimensions.get('window').width
  },
  trackactivity_footer_main_view : {
    flex:1,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center',
    position:'absolute',
    bottom:0,
    resizeMode: 'stretch',
    height:87,zIndex:2,
    width:Dimensions.get('window').width,
    backgroundColor: 'rgba(237, 75, 60, 0.8)'
  },
  trackactivity_footer_main_view2: {
    position:'absolute',
    bottom:0,
    backgroundColor:'#414249',flex:1,flexDirection:'row',paddingTop:12,paddingBottom:2
  }






});


export default styles;
