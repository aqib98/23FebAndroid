/**
* Sample React Native App
* https://github.com/facebook/react-native
* @flow
*/

import React, { Component } from 'react';
import {
Platform,
StyleSheet,
Text,
View,
Switch,
Image,
PixelRatio,
KeyboardAvoidingView,
ScrollView,
TouchableHighlight,
TouchableOpacity,
AsyncStorage,ActivityIndicator
} from 'react-native';

import Carousel, { Pagination,ParallaxImage } from 'react-native-snap-carousel';
import {ApolloProvider,graphql,createNetworkInterface,ApolloClient} from 'react-apollo'
import gql from 'graphql-tag'

import { NavigationActions } from 'react-navigation';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import  Entypo from 'react-native-vector-icons/Entypo'
import EvilIcons from 'react-native-vector-icons/EvilIcons'
import Feather from 'react-native-vector-icons/Feather'

import { LineChart, YAxis } from 'react-native-svg-charts'
import { LinearGradient, Stop } from 'react-native-svg'

import * as shape from 'd3-shape'
const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');



import styles from './styles.js'
import { ENTRIES2 } from './entries';
const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}

const slideHeight = viewportHeight * 0.4;
const slideWidth = wp(90);
const itemHorizontalMargin = wp(2);
const sliderWidth = viewportWidth;
const itemWidth = slideWidth + itemHorizontalMargin * 2;

import { getFeed } from '../../template/Workout/workout.js'

class FeedApp extends Component<{}> {
  constructor(props) {
    super(props);
    this.state = {
      width:0,
      height : 0,
      user:{id:'', name:''},
      slider1Ref :null,
      slider1ActiveSlide: 1,
      feeds:[],
      loader:true
    };
  }

  async componentWillMount(){
    console.log('hihi')
    var state = this.state;
    let USER_ID = await AsyncStorage.getItem("USER_ID");
    let USER_NAME = await AsyncStorage.getItem("USER_NAME");

    state.user.id=USER_ID;
    state.user.name = USER_NAME;
    this.setState(state,()=>{console.log(this.state.user)});


      //this._getFeed(this.state.user.id)

    }

   async componentWillReceiveProps(nextProps){
      console.log('props',nextProps)
      console.log('nextProps',nextProps.data.loading)

      if(nextProps.data.loading===false){
         this._getFeed(this.state.user.id,nextProps.data)
      }
    }




_getFeed=(user_id,data)=>{



  response = data

 feed = response.course_to_user

console.log(feed)


 array = []

 for(i=0;i<feed.length;i++)
 {

  feed_user = feed[i].user_detail[0]

  activity_data = feed_user.activity_log

  workout_data = feed_user.workout_to_user

  if(activity_data != '')
  {
   latest_actvivty_date = activity_data[0].completed_on

   activity_date = new Date(latest_actvivty_date);
  }
  if(workout_data != '')
  {
   workout_activity_date = workout_data[0].completion_date

   workout_date = new Date(workout_activity_date);

  }

  if(workout_date != '' && activity_date != '')
  {
        if (workout_date > activity_date) {

            user_date = workout_date

        }else {

            user_date = activity_date

        }
  }
  else if(workout_date == '')
  {
    user_date = activity_date
  }
  else {
    user_date = workout_date
  }



  array.push([user_date, feed[i], feed[i].user_id])


 }

 array.sort(function(a, b) {
    var dateA = new Date(b[0]), dateB = new Date(a[0]);
    return dateA - dateB;
 });


        var arr = []
        var mani = []
        var feeddata = []

          for(i=0;i<array.length;i++)
          {
            user_detail = array[i][1].user_detail

            userwrokout_data = user_detail[0].workout_to_user
            useractivity_data = user_detail[0].activity_log

            for(j=0;j<userwrokout_data.length;j++)
            {
              userexcierce_data = userwrokout_data[j].workout;
              //arr.push([users[i].user_id, users[i].name,userwrokout_data[j].title,userwrokout_data[j].completion_date,userwrokout_data[j].thumbnail_url,userwrokout_data[j].intensity,userexcierce_data[0].avg_heart_rate])
              arr["user_id"] = array[i][2];
              arr["name"] = user_detail[0].first_name
              arr["title"] = userexcierce_data[0].title
              arr["completion_date"] = userwrokout_data[j].completion_date
              arr["thumbnail_url"] = userexcierce_data[0].thumbnail_url
              arr["intensity"] = userexcierce_data[0].intensity
              arr["heartrate"] = userwrokout_data[j].avg_heart_rate
              arr["completed_date"] = userwrokout_data[j].completion_date
              if(user_detail[0].profile_img_url == null){
                arr["profile_img_url"] = "https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg"
              } else {
                arr["profile_img_url"] =  user_detail[0].profile_img_url
              }


              var today = new Date();
              var Christmas = new Date(userwrokout_data[j].completion_date);
              var diffMs = (today-Christmas); // milliseconds between now & Christmas
              var diffDays = Math.floor(diffMs / 86400000); // days
              var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
              var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
              if(diffDays > 0)
              {
                arr["completion_date"] = diffDays + " days ago";
              } else if( diffHrs > 0) { arr["completion_date"] = diffHrs + " hours ago"; } else { arr["completion_date"] = diffMins + " minutes ago"; }
              var obj = {"user_id":arr["user_id"],"name":arr["name"],"title":arr["title"],"completion_date":arr["completion_date"],"thumbnail_url":arr["thumbnail_url"],"intensity":arr["intensity"],"heartrate":arr["heartrate"],"completed_date":arr["completed_date"],"profile_img_url": arr["profile_img_url"]};
              feeddata.push(obj)
              arr =[]

            }
            for(j=0;j<useractivity_data.length;j++)
            {
              //arr.push([users[i].user_id, users[i].name,useractivity_data[j].title,useractivity_data[j].completed_on,'','',''])
              arr["user_id"] = array[i][2];
              arr["name"] = user_detail[0].first_name
              arr["title"] = useractivity_data[j].title
              arr["completion_date"] = useractivity_data[j].completed_on
              arr["thumbnail_url"] = 'https://cdn-resoltz-assets.azureedge.net/content/images/corporate/onepage/classes-4.jpg'
              arr["intensity"] = '0'
              arr["heartrate"] = useractivity_data[j].avg_heart_rate
              arr["completed_date"] = useractivity_data[j].completed_on
              if(user_detail[0].profile_img_url == null){
                arr["profile_img_url"] = "https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg"
              } else {
                arr["profile_img_url"] =  user_detail[0].profile_img_url
              }


              var today = new Date();
              var Christmas = new Date(useractivity_data[j].completed_on);
              var diffMs = (today-Christmas); // milliseconds between now & Christmas
              var diffDays = Math.floor(diffMs / 86400000); // days
              var diffHrs = Math.floor((diffMs % 86400000) / 3600000); // hours
              var diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000); // minutes
              if(diffDays > 0)
              {
                arr["completion_date"] = diffDays + " days ago";
              } else if( diffHrs > 0) { arr["completion_date"] = diffHrs + " hours ago"; } else { arr["completion_date"] = diffMins + " minutes ago"; }

              var obj = {};
              var len = arr.length;

              var obj = {"user_id":arr["user_id"],"name":arr["name"],"title":arr["title"],"completion_date":arr["completion_date"],"thumbnail_url":arr["thumbnail_url"],"intensity":arr["intensity"],"heartrate":arr["heartrate"],"completed_date":arr["completed_date"], "profile_img_url": arr["profile_img_url"]};
              feeddata.push(obj)
              arr =[]

            }

            feeddata.sort(function(a, b) {
              var dateA = new Date(b.completed_date), dateB = new Date(a.completed_date)
              return dateA - dateB;
            });

            mani.push(['user'+i+'', feeddata])

            feeddata=[]
          }


          this.setState({feeds:mani,loader:false})








}



  _renderWorkout= ({item, index},parallaxProps)=> {

    heartrate = []
    high_heart_rate = null
    let checkHeartRate = true
    if(item.heartrate)
    {

      if(item.heartrate.length===0){
        console.log('heart_rate empty',item.heartrate)
        checkHeartRate = false
        high_heart_rate = 0
      }else {
        console.log('heart_rate not_empty',item.heartrate)
        checkHeartRate = true
        heartrate = item.heartrate
        high_heart_rate = Math.max.apply(null, heartrate)
      }

    }
    const resizeMode = 'center';
    const text = 'This is some text inlaid in an <Image />';

    if(item.intensity < 5)
    {
      var level = "LOW"
    }
    else {
      var level = "HIGH"
    }

    return (
        <View >
        <View style={styles.feed_img_mas}>
          <Image style={{ width: 45, height: 45, marginTop:15, borderRadius: 23 }} source={{uri: item.profile_img_url}} />
          <Text style={styles.feed_title1}>{item.name}</Text>
          <View style={styles.feedrgt_mas}>
            <Text style={styles.feedrgt1}>{item.title}</Text>
            <Text style={styles.feedrgt2}>{item.completion_date}</Text>
          </View>
        </View>
          <TouchableOpacity
              activeOpacity={1}
              style={styles.carousel_style_workout}
              onPress={() => { console.log('hi'); }}
              >

                <View style={{flex: 1,backgroundColor: 'white',}}>

                    <Image
                      style={{
                        position: 'absolute',
                        width: '100%',
                        height: '100%',
                        flexDirection: 'column',
                        justifyContent: 'flex-end',
                      }}
                      source={{ uri: item.thumbnail_url }}
                    >

                    </Image>
                    <LineChart
                        style={ { height: 80, opacity:0.85, backgroundColor: '#333333', marginTop: 150 } }
                        dataPoints={ heartrate }
                        renderGradient={ ({ id }) => (
                            <LinearGradient id={ id } x1={ '0%' } y={ '0%' } x2={ '0%' } y2={ '100%' }>
                                <Stop offset={ '0%' } stopColor={ 'rgb(228, 35, 89)' } stopOpacity={ 0.8 }/>
                                <Stop offset={ '100%' } stopColor={ 'rgb(255, 156, 0)' } stopOpacity={ 0.9 }/>
                            </LinearGradient>
                        ) }
                    />






                </View>
                <View>
                  <Text style={styles.feedrgt2}>{level}</Text>
                  <Text style={styles.feedrgt2}>MAX Heart Rate: {Math.max(high_heart_rate)}</Text>
                </View>

            </TouchableOpacity>

        </View>
    );
}
_refresh () {

  return new Promise((resolve) => {
    setTimeout(()=>{
      resolve();
    }, 4000)
  })
}

render() {
  console.log('in render')
  console.log(this.props.data.loading)
  const {navigation} = this.props;
  const { loader } = this.state;
  const resizeMode = 'center';
  const text = 'I am some centered text';



  return (

    <View style={styles.mainBody} >

        <ScrollView
                  style={{flex:1}}
                  contentContainerStyle={{paddingBottom: 50}}
                  indicatorStyle={'white'}
                  scrollEventThrottle={200}
                  directionalLockEnabled={true}
                >
                  <View style={styles.listofWrkouts1}>


                    <View style={styles.header}>
                          <Text style={styles.topSignupTxt}>
                            Feed
                          </Text>
                    </View>

                    <View>
                      <Text style = {styles.text_workout_heading}>
                        Hey {this.state.user.name}!
                      </Text>
                      <Text style = {styles.text_workout_sub_heading}>
                        REMAINDER TO STAY STRONG AND KEEP YOUR GOALS IN MIND
                      </Text>
                    </View>

                    <View style={styles.slidertop_btns}>
                      <View style={{flex:1,flexDirection:'row',justifyContent:'center',alignItems:'center'}}>
                        <View style = {{flex:0.28,alignItems:'center',justifyContent:'center',}}>
                        <TouchableOpacity>
                          <View style={styles.gradi_btns}>
                            <Text style={styles.gradi_NormalbtnsTxtBtn}>
                                Distance
                            </Text>
                            <View style={styles.gradiPagBtns}></View>
                          </View>
                        </TouchableOpacity>
                        </View>
                        <View style = {{flex:0.28,alignItems:'center',justifyContent:'center',}}>
                        <TouchableOpacity>
                        <View style={styles.gradi_btns}>
                          <Text style={styles.gradi_NormalbtnsTxtBtn}>
                              Weights
                          </Text>
                          <View style={styles.gradiPagBtns}></View>
                        </View>
                        </TouchableOpacity>
                        </View>
                        <View style = {{flex:0.28,alignItems:'center',justifyContent:'center',}}>
                        <TouchableOpacity>
                        <View style={styles.gradi_btns}>
                          <Text style={styles.gradi_btnsTxtBtn}>
                              Conditioning
                          </Text>
                          <Image style={{ width: 86, height: 32 }} source={{uri:'gradi_btn'}} />
                          </View>
                        </TouchableOpacity>
                        </View>
                        <View style = {{flex:0.16,alignItems:'center',justifyContent:'center',}}>
                          <TouchableOpacity >
                              <View>
                                <Feather name="sliders" size={25} color='#fff' />
                              </View>
                          </TouchableOpacity>

                        </View>

                      </View>
                    </View>
                  </View>


                  {loader?
                    <ActivityIndicator
                      animating = {this.state.contentLoader}
                      color = '#bc2b78'
                      size = "large"
                      style = {styles.activityIndicator}
                    />
                  :

                  <ScrollView
                    style={{flex:1, paddingBottom: 10, marginBottom: 20}}
                    contentContainerStyle={{paddingBottom: 50}}
                    indicatorStyle={'white'}
                    scrollEventThrottle={200}
                    directionalLockEnabled={true}
                  >
                  {this.state.feeds.map((data1)=>{
                    // console.log(data1)
                    // console.log(this.state.feeds)
                    const feed_data = data1[1]
                    // console.log(data1[1])
                    return(
                      <View style={{borderBottomWidth:4}}>
                          <Carousel
                            ref={(c) => { if (!this.state.slider1Ref) { this.setState({ slider1Ref: c }); } }}
                            data={feed_data}
                            renderItem={this._renderWorkout}
                            sliderWidth={sliderWidth}
                            itemWidth={230}
                            itemHeight={300}
                            hasParallaxImages={false}
                            firstItem={0}
                            containerCustomStyle={{marginTop: 15}}
                            itemWidth={itemWidth}
                            enableSnap = {true}
                            lockScrollWhileSnapping = {false}
                            onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
                          />
                        </View>
                    )
                  }
                  )}
                  </ScrollView>
                  }

                </ScrollView>




                <View style={styles.btm_tabNavigation}>
                    <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 32, height: 28, top:3, position:'absolute' }} source={{uri:'feedicon_a'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>FEED</Text></View>
                    <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 28, height: 28, top:3, position:'absolute'  }} source={{uri:'profileicon_a'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>PROFILE</Text></View>
                    <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 45, height: 45, marginTop:3 }} source={{uri:'bottom_tab_middlelogo_a'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}> </Text></View>
                    <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 28, height: 31, top:3, position:'absolute'  }} source={{uri:'notificationicon_a'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>NOTIFICATION</Text></View>
                    <View style = {{flex:1,flexDirection:'column',alignItems:'center',justifyContent:'center'}}><Image style={{ width: 28, height: 28, top:3, position:'absolute'  }} source={{uri:'btnmenuicon_a'}} /><Text style={{fontSize:10,color:'#fff', marginTop:12}}>MORE</Text></View>
                </View>
    </View>

  );


}
}

const App = graphql(gql`query getUsers($user_id:Int!) {
  course_to_user(user_id:$user_id) {
    user_id
    course_id
    user_detail {
      id
      first_name
      middle_initial
      last_name
      user_name
      profile_img_url
      workout_to_user{
        workout_id
        user_id
        completion_date
        avg_heart_rate
        workout{
          id
          title
          thumbnail_url
          intensity
        }
      }
      activity_log{
        title
        completed_at
        completed_on
        activity_type
        avg_heart_rate
      }
    }

}}`,{options:({user_id})=>({variables:{user_id}})})(FeedApp)

export default class Feed extends Component<{}> {
  constructor(props){
    super(props)
    this.state={user_id:null}
    const networkInterface = createNetworkInterface('http://resoltz.azurewebsites.net/api/v1/newfeed/user')
    this.client = new ApolloClient({
      networkInterface
    })

  }

  async componentWillMount(){
    console.log('hihi')

    let USER_ID = await AsyncStorage.getItem("USER_ID");
    this.setState({user_id:USER_ID})

  }

  render(){
    return(
      <ApolloProvider client = {this.client}>
        <App user_id={this.state.user_id}/>
      </ApolloProvider>
      )
  }
}
