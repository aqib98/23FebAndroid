import React, { Component } from 'react';
import {
  AppRegistry,
  FlatList,
  StyleSheet,
  TextInput,
  Picker,
  ScrollView,
  TouchableOpacity,
  Text,
  Image,
  KeyboardAvoidingView,
  View,
  TouchableHighlight,
  ToastAndroid,
  Alert,
  ActivityIndicator
} from 'react-native';
import { Card, Button, FormLabel, FormInput, FormValidationMessage } from "react-native-elements";
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import { NavigationActions } from 'react-navigation';
import PhotoUpload from 'react-native-photo-upload';
import { onSignIn, getAllAsyncStroage } from '../../../config/auth';
import { getProfileDetails } from '../../template/SQLiteOperationsOffline.js';
import { updateProfile } from '../../template/api.js';
import  API  from '../../template/constants.js';
import { AsyncStorage } from "react-native";
import {RadioGroup, RadioButton} from 'react-native-flexi-radio-button';
import axios from 'axios';
const Dimensions = require('Dimensions');
const {height, width} = Dimensions.get('window');
import styles from '../Landing/styles.js';
export default class ProfileDetails extends Component {

    constructor(props) {
      super(props);
      this.state={
        profile:[],
        height:null,
        weight:null,
        age:null,
        gender:'Male',
        userid:null,
        genderIndex:null,
        enable:null,
        Loader: false,
        contentLoader:true,
        editable:null,
        profile_img_url:null,
        upload_uri: null,
        upload_uri_check : false,
        visible: true,
        imperial:true,

      }
  }
  componentWillMount(){
  getAllAsyncStroage()
  .then(res =>
    console.log(res)
  )
  .catch(err =>
    console.log(err)
  );
  this.getStudentID();
  console.log(API)
}


  photouploadserver = (file) => {
    console.log('response',file)
    if(file.didCancel===true){
      this.setState({visible:true})
    }else{
      let formData = new FormData();

    if(file.fileName)
    {
       name = file.fileName
    }
    else {
       image_source = file.uri
       image = image_source.split("/");
       image_length = image.length
       name = image[image_length-1];
    }

    console.log(name)

    formData.append('myfile',{type:'image/png',name: name,uri:file.uri.toString()})

      axios.post(API.Image_url, formData ).then(response => {
      updated_img = response.data
      console.log(updated_img)
      this.setState({visible:true})
      this.setState({upload_uri: updated_img.url})
      this.setState({enable:true})



      }).catch(err => {
        console.log('err')
        console.log(err)

      })

    }


  }

//   method: 'post',
//   headers: {
//   'Accept': 'application/json',
//   'Content-Type': 'application/json'
// },
//
// body: JSON.stringify({
//   data: files.data.toString(),
//   fileName: files.fileName
// })
//   )
//       .then((response)=>{
//         console.log('image upload response: ', response);
//         console.log('url : ', response.data.url)
//       })

getStudentID =async()=> {
  console.log('profile view', await AsyncStorage.getItem("DEVICE_NAME"))
    let USER_ID = await AsyncStorage.getItem("USER_ID");
    console.log(USER_ID)
    this.setState({userid:USER_ID,enable:false, Loader:true},()=>{
      this._getStudentDetails(this.state.userid);
    });
  }
  _getStudentDetails=(id)=>{
    getProfileDetails(id).then(response=>{
      console.log(response)
      if(response.status){
        this.setState({profile:response.data},()=>{
          this.setState({profile_img_url: this.state.profile.profile_img_url, height:this.state.profile.height_in_feet, age:this.state.profile.age, weight:this.state.profile.current_weight,gender:this.state.profile.gender},()=>{
            if(this.state.gender==="Male"){
              this.setState({genderIndex:0});
            }else if(this.state.gender==="Female"){
              this.setState({genderIndex:1});
            }else{
              this.setState({genderIndex:null});
            }
          });
        });
        console.log(this.state)
        this.setState({Loader:false, contentLoader:false})
      }else{
        this.setState({Loader:false, contentLoader:false})
        ToastAndroid.show(response.show, ToastAndroid.SHORT);
      }
    },err=>{
      console.log(err)
    })
  }
  addBLERoute=async ()=>{
    const { navigate } = this.props.navigation;
    if(await AsyncStorage.getItem("DEVICE_NAME")&& await AsyncStorage.getItem("DEVICE_ID")){
      navigate('UpcomingWorkouts')
    }else{
      Alert.alert('Do you want to save BLE device?','You can add later also.',
        [
          {text: 'Add later', onPress: () => navigate('UpcomingWorkouts')},
          {text: 'Add', onPress: () => navigate('SearchDevices', {fromRoute:'ProfileDetails', toRoute:'SearchDevices', enableBack:false})},
        ],
        { cancelable: false }
      )
    }
  }

  _savedata=()=>{
    console.log(this.props, typeof this.props.navigation)
    if(typeof this.props.navigation =='undefined'){
      if(this.state.gender&&this.state.age!==''&&this.state.weight!=""&&this.state.height!=""&&this.state.upload_uri!=""){
        if(this.state.enable){
        if(this.state.age<=18||this.state.age>=70){
          ToastAndroid.show("Age should be greater than 18 and less than 70", ToastAndroid.SHORT);
        }else if(this.state.weight<=30||this.state.weight>=200){
          ToastAndroid.show("Enter valid weight in lbs", ToastAndroid.SHORT);
        }else if(this.state.height<=1||this.state.height>=8){
          ToastAndroid.show("Enter valid height in feet", ToastAndroid.SHORT);
        }else{
          console.log('obj')
          console.log({imagePath: this.state.upload_uri, weight:this.state.weight, age:this.state.age, height:this.state.height, gender:this.state.gender, id:this.state.userid })
          this.setState({contentLoader:true})
          updateProfile({imagePath: this.state.upload_uri, weight:this.state.weight, age:this.state.age, height:this.state.height, gender:this.state.gender, id:this.state.userid })
          .then(res=>{
            console.log(res);
            if(res.status){
              this.getStudentID();
              ToastAndroid.show(res.show, ToastAndroid.SHORT);
              this.setState({enable:false,contentLoader:false})

            }else{
              ToastAndroid.show(res.show, ToastAndroid.SHORT);
              this.setState({enable:false,contentLoader:false})
            }
          },err=>{
            console.log(err);
            ToastAndroid.show(err.show, ToastAndroid.SHORT);
          })
        }
      }else{
        ToastAndroid.show("Edit something", ToastAndroid.SHORT);
      }
      }else{
        ToastAndroid.show("Field cant be empty or 0", ToastAndroid.SHORT);
      }
    }else
    {
      var { fromRoute } = this.props.navigation.state.params;
      const { navigate  } = this.props.navigation;
      console.log(this.state)
      if(this.state.enable&&this.state.gender&&this.state.age!==''&&this.state.weight!=""&&this.state.height!=""&&this.state.upload_uri!=""){
        if(this.state.age<=18||this.state.age>=70){
          ToastAndroid.show("Age should be greater than 18 and less than 70", ToastAndroid.SHORT);
        }else if(this.state.weight<=30||this.state.weight>=200){
          ToastAndroid.show("Enter valid weight in lbs", ToastAndroid.SHORT);
        }else if(this.state.height<=1||this.state.height>=8){
          ToastAndroid.show("Enter valid height in feet", ToastAndroid.SHORT);
        }else{
          this.setState({contentLoader:true})
          updateProfile({imagePath: this.state.upload_uri, weight:this.state.weight, age:this.state.age, height:this.state.height, gender:this.state.gender, id:this.state.userid})
          .then(async res=>{
            console.log(res);
            if(res.status){
              this.getStudentID();
              ToastAndroid.show(res.show, ToastAndroid.SHORT);
              console.log(await AsyncStorage.getItem("DEVICE_NAME"), await AsyncStorage.getItem("DEVICE_ID"))
              fromRoute?this.addBLERoute():this.setState({enable:false,contentLoader:false})
            }else{
              ToastAndroid.show(res.show, ToastAndroid.SHORT);
              fromRoute?this.addBLERoute():this.setState({enable:false,contentLoader:false})
            }
          },err=>{
            console.log(err);
            ToastAndroid.show(err.show, ToastAndroid.SHORT);
          })
        }
      }else{
        ToastAndroid.show("Field cant be empty or 0", ToastAndroid.SHORT);
      }
    }
  }
  onSelect(index, value){
    console.log(index)
    if(!index){
      this.setState({enable:true,gender:"Male"})
    }else{
      this.setState({enable:true, gender:"Female"})
    }
  }
  onUnitSelect(index, value){
    console.log(index)
    if(!index){
      this.setState({imperial:false})
    }else{
      this.setState({imperial:true})
    }
  }

  openMenu = async() => {

    const { navigate  } = this.props.navigation;
    navigate("Menu");

  }

  render() {
    var that=this;
    var radio_props = [
      {label: 'Male', value: 0 },
      {label: 'Female', value: 1 }
    ];
    const { height, weight, age, enable, loader, contentLoader, profile_img_url } = this.state;

    if(profile_img_url == null){
       image_path = 'https://www.sparklabs.com/forum/styles/comboot/theme/images/default_avatar.jpg'
    } else {
       image_path = profile_img_url
    }

    return (
      <View style={styles.mainBody}>
        <View style={styles.header}>
          <Text style={styles.topSignupTxt}>
            {this.props.navigation?this.props.navigation.state.params.title:'Profile Details'}
          </Text>
        </View>
        {contentLoader?
          <ActivityIndicator
            animating = {this.state.contentLoader}
            color = '#bc2b78'
            size = "large"
            style = {styles.activityIndicator}
          />
          :
          <ScrollView style={{ marginTop : 20, marginBottom:0, paddingBottom:10, height:500, width:Dimensions.get('window').width }} >
          <KeyboardAvoidingView behavior="padding">
            <View style={styles.signup_temp_form}>
              <View style={styles.body1a}>

              <View style = {{height:150,width:150,borderRadius:75,paddingVertical:30,backgroundColor:'white'}}>
              <PhotoUpload
               onPhotoSelect={avatar => {
                 if (avatar) {
                   //console.log('Image base64 string: ', avatar)
                 }
               }}
               onStart = {start => {
                console.log('start',start)
                //this.setState({contentLoader:true})
                this.setState({visible:false})
               }}
               onCancel = {cancel => {
                 console.log('cancel---------')
                 this.setState({visible:true})
               }}
               onResponse = {response => {
                 this.photouploadserver(response)
               }}
             >
             {this.state.visible?<Image
                 style={{
                   paddingVertical: 30,
                   width: 150,
                   height: 150,
                   borderRadius: 75,

                 }}
                 resizeMode='cover'
                 source={{ uri: image_path }}
               />:<View style={{height:150,width:150,borderRadius:75,paddingVertical:30,backgroundColor:'white'}}><ActivityIndicator
                animating = {true}
                color = '#cccccc'
                size = "large"
                style = {{paddingTop:26}}
              /></View> }
            </PhotoUpload>

            </View>
            <Text style={{textAlign: 'left', alignSelf: 'center',color:'#808080',marginBottom:15}}>Click on Image to update</Text>

                <Text style={{textAlign: 'left', alignSelf: 'stretch',marginLeft:'15%',color:'#ffffff',marginBottom:5}}>Unit: </Text>
                <RadioGroup
                selectedIndex={this.state.imperial}
                style={{ flexDirection:'row', marginLeft: 10,marginBottom:10 }}
                onSelect = {(index, value) => this.onUnitSelect(index, value)}>
                  <RadioButton value={'Male'} >
                    <Text>Metric</Text>
                  </RadioButton>
                  <RadioButton value={'Female'}>
                    <Text>Imperial</Text>
                  </RadioButton>
                </RadioGroup>
                <Text style={{textAlign: 'left', alignSelf: 'stretch',marginLeft:'15%',color:'#ffffff',marginBottom:5}}>Weight: </Text>
                {this.state.imperial?<TextInput
                  placeholder="Weight (lbs)"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={weight?String(weight):0}
                  placeholderTextColor='#626264'
                  style={styles.textInput_signup}
                  onChangeText={ (text) => this.setState({weight:text, enable:true}) }
                />:<TextInput
                  placeholder="Weight (kgs)"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={weight?String(weight):0}
                  placeholderTextColor='#626264'
                  style={styles.textInput_signup}
                  onChangeText={ (text) => this.setState({weight:text, enable:true}) }
                />}

                <Text style={{textAlign: 'left', alignSelf: 'stretch',marginLeft:'15%',color:'#ffffff',marginBottom:5}}>Height: </Text>
                {this.state.imperial?<TextInput
                  placeholder="Height (Feet)"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={height?String(height):0}
                  placeholderTextColor='#626264'
                  style={styles.textInput_signup}
                  onChangeText={ (text) => this.setState({height:text, enable:true}) }
                />:<View style={{marginRight:40,marginLeft:40,marginTop:0,marginBottom:20,paddingTop:11,paddingBottom:12,paddingLeft:25,borderRadius:30,position:'relative',color: '#fff',width:242,}}>
                  <View style = {{flex:1,flexDirection:'row'}}>
                  <TextInput
                  placeholder="feet"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={height?String(height):0}
                  placeholderTextColor='#626264'
                  style={{paddingLeft:15,marginTop:0,paddingTop:11,backgroundColor:'#202124',borderRadius:25,color: '#fff',width:100,}}
                  onChangeText={ (text) => this.setState({height:text, enable:true}) }
                />
                <Text style = {{marginTop:10,color:'#ffff',fontSize:20}}> : </Text>
                <TextInput
                  placeholder="inches"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={height?String(height):0}
                  placeholderTextColor='#626264'
                  style={{paddingLeft:15,marginTop:0,paddingTop:11,backgroundColor:'#202124',borderRadius:30,color: '#fff',width:100,}}
                  onChangeText={ (text) => this.setState({height:text, enable:true}) }
                />
                </View>
                </View>}

                <Text style={{textAlign: 'left', alignSelf: 'stretch',marginLeft:'15%',color:'#ffffff',marginBottom:5}}>Age: </Text>

                <TextInput
                  placeholder="Age"
                  underlineColorAndroid='transparent'
                  autoCorrect={false}
                  value={age?String(age):0}
                  placeholderTextColor='#626264'
                  style={styles.textInput_signup}
                  onChangeText={ (text) => this.setState({age:text, enable:true}) }
                />
                <Text style={{textAlign: 'left', alignSelf: 'stretch',marginLeft:'15%',color:'#ffffff',marginBottom:5}}>Gender: </Text>

                <RadioGroup
                selectedIndex={this.state.genderIndex}
                style={{ flexDirection:'row', marginLeft: 10 }}
                onSelect = {(index, value) => this.onSelect(index, value)}>
                  <RadioButton value={'Male'} >
                    <Text>Male</Text>
                  </RadioButton>
                  <RadioButton value={'Female'}>
                    <Text>Female</Text>
                  </RadioButton>
                </RadioGroup>


              </View>
            </View>
          </KeyboardAvoidingView>
          </ScrollView>
        }
        {this.props.from==null?null
          :
          this.props.from==='tabs'?
          <TouchableOpacity onPress={() => that._savedata()}>
            <View style={styles.saveBtn2}>
              {loader?
                <ActivityIndicator
                  animating = {this.state.loader}
                  color = '#bc2b78'
                  size = "large"
                  style = {styles.activityIndicator}
                />
                :
                <Text style={styles.saveTxtBtn}>
                  UPDATE
                </Text>
              }
              <Image style={{ width: 200, height: 50 }} source={{uri:'white_btn_bg'}} />
            </View>
          </TouchableOpacity>
          :
          null
        }
        <View style={styles.footer}>
          <TouchableOpacity onPress={ () => this._savedata() }>
            <View style={styles.save_view}>
            {loader?
                <ActivityIndicator
                  animating = {this.state.loader}
                  color = '#bc2b78'
                  size = "large"
                  style = {styles.activityIndicator}
                />
                :
              <Text ></Text>}
            </View>
            <Image
              style={styles.save_btnImg}
              source={{ uri: 'buttonimg' }}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

module.exports = ProfileDetails;
