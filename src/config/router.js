import React from "react";
import { Platform, StatusBar, HeaderRight, StyleSheet, Button, Image } from "react-native";
import { StackNavigator, TabNavigator } from "react-navigation";
import Landing from "../components/screens/Landing/landing";
import Login from "../components/screens/Landing/login";
import Signup from "../components/screens/Landing/signup";
import Courses from "../components/screens/Courses/courses";
import Subscribe from "../components/screens/Courses/subscribe";
import SearchDevices from "../components/screens/Landing/searchdevices";
import UpcomingWorkouts from "../components/screens/Activity/upcomingWorkouts.js";
import WorkoutDetails from "../components/screens/workoutDetails";
import FinishExcierce from "../components/screens/finishexercise";
import Trackactivity from "../components/screens/Activity/trackactivity";
import Logactivity from "../components/screens/Activity/logactivity";
import Videoactivity from "../components/screens/Activity/videoactivity";
import ProfileDetails from "../components/screens/Tabs/profiledetails";
import WorkoutSummary from "../components/screens/workoutSummary";
import TrackActivitySummary from "../components/screens/trackActivitySummary";
import DeviceConnected from "../components/screens/deviceconnected";
import Tabs from '../components/screens/Tabs/tabs';
const styles = StyleSheet.create({
  container: {
   flex: 1,
   justifyContent: 'center',
  },
  list: {
    marginTop:10,
    margin: 1,
    borderColor: '#000',
    borderBottomWidth: 5,
    flex: 1
  },
  item: {
    fontSize: 20,
    paddingVertical: 5,
    color: '#000',
    borderColor: '#000',
    borderBottomWidth: 1,
    flex: 1
  },
  status: {
    textAlign:'right',
    fontWeight: 'bold'
  },
  avatar: {
    height: 35,
    width: 35,
    marginLeft: 15
  }
})

const headerStyle = {

};

export const SignedOut = StackNavigator({

  Landing:{
    screen: Landing,
    navigationOptions: {
      title: "Welcome",
      header: null,
    }
  },
  Login: {
    screen: Login,
    navigationOptions: {
      title: "Log In",
      header: null,
    }
  },
  Signup: {
    screen: Signup,
    navigationOptions: {
      title: "Sign Up",
      header:null
    }
  }
});



export const SignedIn = StackNavigator({
  Tabs: {
      screen: Tabs,
      navigationOptions: {
        title: "Tabs",
        header: null
      }
  },
  Courses: {
      screen: Courses,
      navigationOptions: {
          title: "Courses",
          header: null
      }
  },
  Subscribe:{
    screen : Subscribe,
    navigationOptions:{
      title:'Subsrcibe',
      header:null
    }
  },
  UpcomingWorkouts: {
    screen: UpcomingWorkouts,
    navigationOptions: {
        title: "Courses",
        header: null
    }
  },
  TrackActivitySummary: {
    screen: TrackActivitySummary,
    navigationOptions: {
        title: "TrackActivitySummary",
        header: null
    }
  },
  ProfileDetails: {
    screen: ProfileDetails,
    navigationOptions: ({navigation}) => ({
      title: `${navigation.state.params.title}`,
      header: null,
    }),
  },
  Trackactivity: {
    screen: Trackactivity,
    navigationOptions: {
        title: "Courses",
        header: null
    }
  },
  Logactivity: {
    screen: Logactivity,
    navigationOptions: {
        title: "Courses",
        header: null
    }
  },
  Courses: {
    screen: Courses,
    navigationOptions: {
        title: "Courses",
        header: null
    }
  },
  Videoactivity: {
    screen: Videoactivity,
    navigationOptions: {
        title: "Courses",
        header: null
    }
  },
   WorkoutSummary: {
    screen: WorkoutSummary,
    navigationOptions: {
        title: "Courses",
        header: null
    }
  },
  FinishExcierce: {
    screen: FinishExcierce,
    navigationOptions: {
        title: "Courses",
        header: null
    }
  },
  WorkoutDetails: {
    screen: WorkoutDetails,
    navigationOptions: {
        title: "Courses",
        header: null
    }
  },
  SearchDevices: {
    screen: SearchDevices,
    navigationOptions: {
      title: "SearchDevices",
      header: null
    }
  },
  DeviceConnected: {
    screen: DeviceConnected,
    navigationOptions: {
      title: "DeviceConnected",
      header: null
    }
  }



/*    Courses: {
        screen: Courses,
        navigationOptions: {
            title: "Courses",
            header: null
        }
    },
    Workouts: {
        screen: Workouts,
        navigationOptions: ({navigation}) => ({
          title: `${navigation.state.params.courseTitle}`,
          header: null,
        }),
    },
    Exercises: {
        screen: Exercises,
        navigationOptions: ({navigation}) => ({
          title: `${navigation.state.params.exeTitle}`,
          header: null
        }),
    },
    ExerciseDetails: {
      screen: ExerciseDetails,
        navigationOptions: ({navigation}) => ({
          title: `${navigation.state.params.exeTitle}`,
          header: null
        }),
    },
    Menu: {
      screen: Menu,
      navigationOptions: {
        title: "Menu",
        header: null
      }
    },
    Profile: {
      screen: Profile,
      navigationOptions: {
        title: "Profile",
        header: null,
      }
    },
    BluetoothIntegrate: {
      screen: BluetoothIntegrate,
      navigationOptions: {
        title: "BluetoothIntegrate",
        header: null,
      }
    },
    Heartrate: {
      screen: Heartrate,
      navigationOptions: {
        title: "Heartrate",
        header: null,
      }
    },
    Instructer: {
      screen: Instructer,
      navigationOptions: {
        title: "Instructer ",
        header: null,
      }
    }

*/

});




export const createRootNavigator = (signedIn = false) => {
  return StackNavigator(
    {
      SignedIn: {
        screen: SignedIn,
        navigationOptions: {
          gesturesEnabled: false
        }
      },
      SignedOut: {
        screen: SignedOut,
        navigationOptions: {
          gesturesEnabled: false
        }
      }
    },
    {
      headerMode: "none",
      mode: "modal",
      initialRouteName: signedIn ? "SignedIn" : "SignedOut"
    }
  );
};
